package main

import (
	"fmt"

	"gitlab.com/mochamethod/go_tutorial/stringutil"
)

func main() {
	fmt.Println(stringutil.Reverse(".dlrow ,olleh"))
}
